import db from "../modules/db";

class AuthRepository {
  // implement singleton pattern
  private static instance: AuthRepository;

  private constructor() {}
  public static getAuthRepository(): AuthRepository {
    if (!AuthRepository.instance) {
      return new AuthRepository();
    }
    return AuthRepository.instance;
  }
  public getOneCondition() {
    return 'One condition!';
  }

  public async getAllUsers() {
    return await db.post.findMany({})
  }
}

export default AuthRepository.getAuthRepository();
