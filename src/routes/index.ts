import express from 'express';
import testingAPI from '../controllers/testingController';
import getListUsers from '../controllers/getListUser';

const router = express.Router();
// router.route('/:key').get(DeviceController.getOneDeviceInfo);
// router.route('/').get(DeviceController.getAllDeviceInfo).post(DeviceController.createDevice);
// router.route('/:key/state').get(DeviceController.getOneDeviceState).post(DeviceController.createDeviceState);
// router.route('/state').get(DeviceController.getAllDeviceState);
router.route('/testing').get(testingAPI);
router.route('/users').get(getListUsers);
export default router;
