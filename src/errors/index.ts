import BadRequestError from './badRequest';
import NotFoundError from './notFound';
import UnauthorizedError from './unauthorizied';
import CustomeError from './customErrors';
const Errors = {
  BadRequestError,
  NotFoundError,
  UnauthorizedError,
  CustomeError,
};

export default Errors;
