import {Request, Response} from 'express';
import status from 'http-status';
import AuthRepository from '../repositories/AuthRepository';

async function getListUsers(req: Request, res: Response) {
  const users = await AuthRepository.getAllUsers();
  res.status(status.OK).json({users});
}
export default getListUsers;