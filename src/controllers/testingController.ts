import {Request, Response} from 'express';
import status from 'http-status';

async function testingAPI(req: Request, res: Response) {
  res.status(status.OK).json({testing: 'auth is working!'});
}

export default testingAPI;