import db from '../../src/modules/db';

const run = async () => {
  await db.post.createMany({
    data:[
      {
        id: '1234-5678-9123-4',
        slug: 'ultimate-node-stack',
        title: 'Ultimate-node-stack 2023',
        publishedAt: new Date()
      },
      {
        id: '1234-5678-9123-5',
        slug: 'draft node',
        title: 'Draft node',
      }
    ]
  })
}

// Auto-run when called directly
if(require.main === module){
  run().then( ()=>{
    console.log('Data seed completed');
    process.exit();
  })
  .catch( (err)=> {
    console.log('Creating fail!');
    console.log(err);
  })
}